Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'volunteers#index'
  resources :posts
  resources :users
  resources :clients do
    scope module: :clients do
      resources :tasks do
        post 'cancel'
      end
    end
  end
  resources :volunteers do
    get 'show_confirm', on: :collection
    post 'confirm', on: :collection
  end
  resources :volunteer_imports, only: [:new, :create]

  resources :tasks, only: [:index, :new, :create, :show] do
    post 'accept'
    post 'finish'
  end

  mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?
end
