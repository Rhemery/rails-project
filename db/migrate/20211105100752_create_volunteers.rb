class CreateVolunteers < ActiveRecord::Migration[6.1]
  def change
    create_table :volunteers do |t|
      t.string :name
      t.string :email
      t.string :token, index: { unique: true }
      t.boolean :is_confirmed, default: false
      t.timestamps
    end
  end
end
