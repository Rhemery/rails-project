class AddUnigueIndexToVolunteers < ActiveRecord::Migration[6.1]
  def change
    add_index :volunteers, :email, unique: true
  end
end
