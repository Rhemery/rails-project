class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.references :client, foreign_key: true
      t.references :volunteer, foreign_key: true
      t.string :task_name
      t.text :notes
      t.timestamps
    end
  end
end
