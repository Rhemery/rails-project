class VolunteerMailer < ApplicationMailer
  def new_magic_link(address, magic_link)
    @magic_link = magic_link

    mail(to: address, subject: "Volunteer invite!")
  end
end
