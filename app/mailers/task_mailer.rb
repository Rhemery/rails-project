class TaskMailer < ApplicationMailer
  def new_task(volunteer, task)
    @task = task
    @volunteer = volunteer
    mail(to: @volunteer.email, subject: "New task available!")
  end
end
