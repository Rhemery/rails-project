class Volunteer < ApplicationRecord
  has_many :tasks, dependent: :nullify, inverse_of: :volunteer
  before_create :generate_token
  scope :with_confirmed, -> { where(is_confirmed: true) }
  scope :not_deleted, -> { where(deleted_at: nil) }
  scope :ordered_by_name, -> { order(name: :asc) }

  validates :email, presence: true, uniqueness: true

  def generate_token
    self.token = SecureRandom.hex
  end
end
