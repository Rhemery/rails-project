class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def self.human_enum(enum_name)
    send(enum_name).transform_keys do |key|
      I18n.t("activerecord.attributes.#{model_name.i18n_key}.#{enum_name}.#{key}")
    end
  end
end
