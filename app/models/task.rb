class Task < ApplicationRecord
  belongs_to :client, inverse_of: :tasks
  belongs_to :volunteer, inverse_of: :tasks, optional: true
  scope :ordered_by_created_date, -> { order(created_at: :asc) }

  enum status: { 
    pending: 0,
    in_progress: 1,
    finished: 2,
    cancelled: 3
   }

  validates :task_name, :notes, presence: true
  def taken?
    !volunteer.nil?
  end
end
