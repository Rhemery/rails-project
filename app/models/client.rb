class Client < ApplicationRecord
  has_many :tasks, -> { order(created_at: :desc) }, dependent: :destroy, inverse_of: :client
  validates :name, :phone_number, :address, presence: true
end
