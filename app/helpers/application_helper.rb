module ApplicationHelper
    def attribute_info(record, name)
        "#{record.class.human_attribute_name(name)}: #{record.send(name)}"
    end
end
