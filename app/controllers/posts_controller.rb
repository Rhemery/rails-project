class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  def index
    @posts = Post.all.includes :user
  end
  def new
    @post = Post.new
  end
  def create
    @post = Post.new posts_params
    if @post.save
      redirect_to post_path(@post)
    else
      render :new
    end
  end
  def show
    #set_post()
  end
  def edit
    #set_post()
  end
  def update
    if @post.update posts_params
      redirect_to post_path(@post)
    else
      render :edit
    end
  end
  def destroy
    @post.destroy
    redirect_to posts_path
  end


  private
  def set_post
    @post = Post.find(params[:id])
  end
  def posts_params
    params.require(:post).permit(:name, :text, :user_id)
  end
end
