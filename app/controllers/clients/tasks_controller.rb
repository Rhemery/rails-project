class Clients::TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy]
  before_action :set_client

  def new
    @task = Task.new
  end
  def create
    @task = @client.tasks.new tasks_params
    if @task.save
      volunteers = Volunteer.with_confirmed
      volunteers.each do |volunteer|
        #magic_link = client_task_path(@client, @task, token: volunteer.token)
        TaskMailer.new_task(volunteer, @task).deliver_later
      end
      redirect_to client_task_path(@client, @task)
    else
      render :new
    end
  end
  def show
    #set_task()
  end
  def edit
    #set_task()
  end
  def update
    if @task.update tasks_params
      redirect_to client_task_path(@client, @task)
    else
      render :edit
    end
  end

  def destroy
    @task.destroy
    redirect_to tasks_path
  end

  def cancel
    @task = Task.find(params[:task_id])
    @task.update! status: :cancelled
    redirect_to client_task_path(@client, @task)
  end


  private
  def send_email(volunteer)
    magic_link = show_confirm_volunteers_path(token: volunteer.token)
    VolunteerMailer.new_magic_link(volunteer.email, magic_link).deliver_later
  end
  def set_task
    @task = Task.find(params[:id])
  end
  def set_client
    @client = Client.find(params[:client_id])
  end
  def tasks_params
    params.require(:task).permit(:task_name, :notes, :volunteer_id)
  end
end
