class ClientsController < ApplicationController
  before_action :set_client, only: [:show, :edit, :update, :destroy]

  def index
    @clients = Client.all
  end
  def new
    @client = Client.new
  end
  def create
    @client = Client.new clients_params
    if @client.save
      redirect_to client_path(@client)
    else
      render :new
    end
  end
  def show
    if params[:status_filter].present?
      @tasks = @client.tasks.where(status: params[:status_filter])
    else
      @tasks = @client.tasks
    end
  end
  def edit
    #set_client()
  end
  def update
    if @client.update clients_params
      redirect_to client_path(@client)
    else
      render :edit
    end
  end
  def destroy
    @client.destroy
    redirect_to clients_path
  end


  private
  def set_client
    @client = Client.find(params[:id])
  end
  def clients_params
    params.require(:client).permit(:name, :phone_number, :address, :access_info, :notes)
  end
end
