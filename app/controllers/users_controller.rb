class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.all
  end
  def new
    @user = User.new
  end
  def create
    @user = User.new users_params
    if @user.save
      redirect_to user_path(@user)
    else
      render :new
    end
  end
  def show
    #set_user()
  end
  def edit
    #set_user()
  end
  def update
    if @user.update users_params
      redirect_to user_path(@user)
    else
      render :edit
    end
  end
  def destroy
    @user.destroy
    redirect_to users_path
  end


  private
  def set_user
    @user = User.find(params[:id])
  end
  def users_params
    params.require(:user).permit(:name, :email, :phone_number)
  end
end
