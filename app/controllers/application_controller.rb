class ApplicationController < ActionController::Base
  before_action :set_volunteer_from_token
  before_action :check_admin, :check_login
  helper_method :current_user, :is_admin?, :is_volunteer?

  def set_volunteer_from_token
    if params[:token].present?
      session[:current_user] = Volunteer.find_by(token: params[:token]).id
    end
  end
  def current_user
    Volunteer.find(session[:current_user])
  end
  def check_admin
    authenticate_or_request_with_http_basic do |username, password|
      if username == 'username' && password == 'password'
        session[:is_admin] = true
      else
        session[:is_admin] = false
      end
    end
  end
  def check_login
    if is_admin? || is_volunteer?
      return true
    else
      render status: :unauthorized, plain: "Unauthorized"
    end
  end
  def is_admin?
    session[:is_admin]
  end
  def is_volunteer?
    !session[:current_user].nil?
  end
end
