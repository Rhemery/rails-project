class VolunteersController < ApplicationController
  before_action :set_volunteer, only: [:show, :edit, :update, :destroy]
  skip_before_action :check_admin, only: [:show_confirm, :confirm]

  def index
    @volunteers = Volunteer.not_deleted
  end

  def new
    @volunteer = Volunteer.new
  end

  def create
    @volunteer = Volunteer.new volunteers_params
    if @volunteer.save
      send_email
      redirect_to volunteer_path(@volunteer)
    else
      render :new
    end
  end

  def show
    # set_volunteer()
  end

  def edit
    # set_volunteer()
  end

  def update
    if @volunteer.update volunteers_params
      redirect_to volunteer_path(@volunteer)
    else
      render :edit
    end
  end

  def destroy
    if @volunteer.deleted_at == nil
      @volunteer.update! is_confirmed: false, deleted_at: Time.now
    end
    redirect_to volunteers_path
  end

  def show_confirm
    @volunteer = Volunteer.find_by!(token: params[:token])
  end

  def confirm
    @volunteer = Volunteer.find_by!(token: params[:token])
    if !@volunteer.is_confirmed
      @volunteer.update! name: params[:name], is_confirmed: true
    end
    redirect_to tasks_path
  end

private

  def send_email
    magic_link = show_confirm_volunteers_url(token: @volunteer.token)
    VolunteerMailer.new_magic_link(@volunteer.email, magic_link).deliver_now
  end

  def set_volunteer
    @volunteer = Volunteer.find(params[:id])
  end

  def volunteers_params
    params.require(:volunteer).permit(:name, :email)
  end
end
