class TasksController < ApplicationController
    skip_before_action :check_admin, except: [:new, :create]

    def new
        @task = Task.new(client_id: params[:client_id])
        @clients = Client.order(name: :asc)
    end
    def create
        @task = Task.new tasks_params
        if @task.save
            volunteers = Volunteer.with_confirmed
            volunteers.each do |volunteer|
                TaskMailer.new_task(volunteer, @task).deliver_later
            end
            redirect_to client_task_path(@task.client, @task)
        else
            render :new
        end
    end

    def index
      @tasks_in_progress = current_user.tasks.in_progress
      @tasks_available = Task.ordered_by_created_date.pending
    end

    def show
        @task = Task.find(params[:id])
    end

    def accept
        @task = Task.find(params[:task_id])
        if !@task.taken?
            @task.update! volunteer: current_user, status: :in_progress
        end
        redirect_to task_path(@task)
    end
    def finish
        @task = Task.find(params[:task_id])
        @task.update! status: :finished
        redirect_to task_path(@task)
    end

    private

    def tasks_params
        params.require(:task).permit(:client_id, :task_name, :notes)
    end
end
  