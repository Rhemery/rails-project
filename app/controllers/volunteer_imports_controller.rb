class VolunteerImportsController < ApplicationController
  def new

  end
  def create
    @emails = params[:emails].lines
    volunteers = nil
    Volunteer.transaction do
      valid_volunteers = []
      @emails.each do |email|
        volunteer = Volunteer.new(email: email)
        valid_volunteers << volunteer if volunteer.save
      end
    end
    valid_volunteers.each do |volunteer|
      send_email(volunteer)
    end
    redirect_to root_path

  end
  def send_email(volunteer)
    magic_link = show_confirm_volunteers_path(token: volunteer.token)
    VolunteerMailer.new_magic_link(volunteer.email, magic_link).deliver_later
  end
end
